# ics-ans-role-cadvisor

Ansible role to install cAdvisor.

## Role Variables

```yaml
cadvisor_container_name: cadvisor
cadvisor_image: google/cadvisor:latest
cadvisor_port: 9191
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-cadvisor
```

## License

BSD 2-clause
